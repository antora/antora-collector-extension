= Antora Collector Extension Documentation

The Antora Collector extension augments the content aggregation capabilities of Antora by invoking external commands that generate and/or retrieve files that can be imported into the content aggregate.

== Overview

Antora Collector provides an extension that enables you to generate files and import them into the content aggregate.
The purpose of this extension is to contribute additional files to content source roots to supplement the files discovered from the conventional content source roots Antora has already loaded.

This activity is carried out by a Collector instance, which is defined per content source root.
A Collector instance can delegate to external commands to generate (or otherwise retrieve) files using a run action.
It can then import those files into the content aggregate using a scan action.
The extension provides numerous settings to configure this behavior.
Each Collector instance performs actions in serial, and Collector instances are invoked in serial as well.

This extension runs during Antora's `contentAggregated` event, which means the extension operates on and contributes to the content aggregate, before the content is classified.
The scanned files aren't treated any differently than files discovered in a content source root defined in the playbook.
Once the files are imported by Collector, those files can be used as though they were written by a human and saved in a git tree or local worktree.
That means they can be referenced just as any other resource in Antora.

You can think of Collector as adding additional content sources to your playbook that happen to be created dynamically (and which are transient).
In fact, Collector can even discover and import files from ad-hoc layouts, unlike a conventional content source.

The extension is agnostic to the build tool (Gradle, Maven, npm, etc.) and--to the extent possible--the operating system.

== Features

The Antora Collector extension provides the following capabilities and features:

* Automatically prepares a worktree per content source root (i.e., origin), if necessary.
* Runs external commands per content source to generate or gather files to add to the content aggregate.
* Scans for files to import into the content aggregate.
* Remaps files during import that are not organized according to the standard Antora directory structure.
* Creates additional component versions (and thus components), if necessary.
* Can replace the version of the current component version with a generated value.
* Populates the current component version with additional metadata read from a generated antora.yml file.
* Allows generated files to be retained for use by another extension that runs later in the pipeline.

== Get started

To get an idea of how Collector works and what you can accomplish with it, refer to xref:concepts.adoc[].
You can get started with Collector by xref:install.adoc[installing] and xref:register.adoc[registering] the extension.
You'll then be ready to learn how to xref:configure.adoc[configure] instances of Collector.

Once you understand how to work with the Collector extension, check out the xref:use-cases.adoc[use cases] to get ideas about how it can be used.
