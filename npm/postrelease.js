'use strict'

const { promises: fsp } = require('node:fs')
const ospath = require('node:path')

const PROJECT_ROOT_DIR = ospath.join(__dirname, '..')
const DOCS_CONFIG_FILE = ospath.join(PROJECT_ROOT_DIR, 'docs/collector-extension/antora.yml')
const VERSION = process.env.npm_package_version

function updateDocsConfig () {
  const hyphenIdx = VERSION.indexOf('-')
  const main = ~hyphenIdx ? VERSION.slice(0, hyphenIdx) : VERSION
  const prerelease = ~hyphenIdx ? VERSION.slice(hyphenIdx + 1) : undefined
  const [major, minor, patch] = main.split('.')
  return fsp.readFile(DOCS_CONFIG_FILE, 'utf8').then((docsConfig) => {
    docsConfig = docsConfig
      .replace(/^version: \S+$/m, `version: ${q(major + '.' + minor)}`)
      .replace(/^prerelease: \S+$/m, `prerelease: ${prerelease ? q('.' + patch + '-' + prerelease) : 'false'}`)
    return fsp.writeFile(DOCS_CONFIG_FILE, docsConfig, 'utf8')
  })
}

function q (str) {
  return `'${str}'`
}

;(async () => {
  await updateDocsConfig()
})()
