'use strict'

const fs = require('node:fs')
const url = require('node:url')

fs.mkdirSync('reports', { recursive: true })

process.on('exit', () => {
  if (process.exitCode) return
  logCoverageReportPath()
})

function logCoverageReportPath () {
  const coverageReportRelpath = 'reports/lcov-report/index.html'
  if (!(process.env.NODE_V8_COVERAGE && fs.existsSync(coverageReportRelpath))) return
  const { CI_PROJECT_PATH, CI_JOB_ID } = process.env
  const coverageReportURL = CI_JOB_ID
    ? `https://gitlab.com/${CI_PROJECT_PATH}/-/jobs/${CI_JOB_ID}/artifacts/file/${coverageReportRelpath}`
    : url.pathToFileURL(coverageReportRelpath)
  console.log(`Coverage report: ${coverageReportURL}`)
}
