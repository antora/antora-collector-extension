'use strict'

process.env.NODE_ENV = 'test'

const assert = require('node:assert/strict')
const { describe, before, beforeEach, after, afterEach, it } = require('node:test')
const { configureLogger } = require('@antora/logger')
const fs = require('node:fs')
const { promises: fsp } = fs
const { Git: GitServer } = require('node-git-server')
const { once } = require('node:events')
const os = require('node:os')
const ospath = require('node:path')
const yaml = require('js-yaml')

beforeEach(() => configureLogger({ level: 'silent' }))

const assertx = {
  contents: (actual, expected, msg) => {
    assertx.file(actual)
    assert.equal(fs.readFileSync(actual, 'utf8'), expected, msg)
  },
  deepEqualSubset: (actual, expected, msg) =>
    assert.deepEqual(sliceObject(actual, ...Object.keys(expected)), expected, msg),
  directory: (actual) => {
    let stat
    try {
      stat = fs.statSync(actual)
    } catch {
      stat = { isDirectory: () => false }
    }
    assert(stat.isDirectory(), `Expected value to be a directory: ${actual}`)
  },
  notDirectory: (actual) => {
    let stat
    try {
      stat = fs.statSync(actual)
    } catch {
      stat = { isDirectory: () => false }
    }
    assert(!stat.isDirectory(), `Expected value to not be a directory: ${actual}`)
  },
  empty: (actual) => assert.equal(actual.length, 0, 'Expected value to be empty'),
  file: (actual) => {
    let stat
    try {
      stat = fs.statSync(actual)
    } catch {
      stat = { isFile: () => false }
    }
    assert(stat.isFile(), `Expected value to be a file: ${actual}`)
  },
}

async function captureStdStream (streamName, fn) {
  const streamPropertyDescriptor = Object.getOwnPropertyDescriptor(process, streamName)
  let streamFile, streamFh
  try {
    streamFile = await mkftempForStdStream(streamName)
    streamFh = await fsp.open(streamFile, 'w', 0o600)
    Object.defineProperty(process, streamName, { get: () => streamFh })
    await fn()
    Object.defineProperty(process, streamName, streamPropertyDescriptor)
    return fsp.readFile(streamFile, 'utf8')
  } catch (err) {
    Object.defineProperty(process, streamName, streamPropertyDescriptor)
    if (streamFh && streamName === 'stderr') Object.assign(err, { stderr: await fsp.readFile(streamFile, 'utf8') })
    throw err
  } finally {
    if (streamFh) {
      if (!streamFh.closed) await streamFh.close().catch(() => undefined)
      await fsp.rm(streamFile, { force: true })
      await fsp.rmdir(streamFile.slice(0, streamFile.length - ('.' + streamName).length), { force: true })
    }
  }
}

const captureStderr = captureStdStream.bind(null, 'stderr')

const captureStdout = captureStdStream.bind(null, 'stdout')

function closeServer (server) {
  return once(server.close() || server, 'close')
}

function getFixtureBuilder (testFilename) {
  const basePath = ospath.join(ospath.dirname(testFilename), 'fixtures', ospath.basename(testFilename, '.js'))
  return (...pathSegments) => ospath.join(basePath, ...pathSegments)
}

function heredoc (literals, ...values) {
  const str =
    literals.length > 1
      ? values.reduce((accum, value, idx) => accum + value + literals[idx + 1], literals[0])
      : literals[0]
  const lines = str.trimRight().split(/^/m)
  if (lines.length < 2) return str
  if (lines[0] === '\n') lines.shift()
  const indentRx = /^ +/
  const indentSize = Math.min(...lines.filter((l) => l.startsWith(' ')).map((l) => l.match(indentRx)[0].length))
  return (indentSize ? lines.map((l) => (l.startsWith(' ') ? l.slice(indentSize) : l)) : lines).join('')
}

// use file adjacent to temporary directory to avoid removal problems on Windows; directory acts as seed
function mkftempForStdStream (streamName) {
  return fsp.mkdtemp(ospath.join(os.tmpdir(), 'stdio-')).then((dir) => dir + '.' + streamName)
}

function sliceObject (obj, ...keys) {
  const result = {}
  for (const k of keys) result[k] = obj[k]
  return result
}

function startGitServer (dir) {
  return new Promise((resolve, reject) => {
    const gitServer = new GitServer(dir, { autoCreate: false })
    gitServer.listen(0, { type: 'http' }, function (err) {
      err ? reject(err) : resolve([gitServer, this.address().port])
    })
  })
}

function trapAsyncError (fn) {
  return fn().then(
    (returnValue) => () => returnValue,
    (err) => () => {
      throw err
    }
  )
}

async function updateYamlFile (filepath, data) {
  const parsed = yaml.load(await fsp.readFile(filepath), { schema: yaml.CORE_SCHEMA })
  Object.assign(parsed, data)
  await fsp.writeFile(filepath, yaml.dump(parsed, { forceQuotes: true, noArrayIndent: true }), 'utf8')
}

module.exports = {
  after,
  afterEach,
  assert,
  assertx,
  before,
  beforeEach,
  captureStderr,
  captureStdout,
  closeServer,
  describe,
  getFixtureBuilder,
  heredoc,
  it,
  updateYamlFile,
  startGitServer,
  trapAsyncError,
  windows: process.platform === 'win32',
}
