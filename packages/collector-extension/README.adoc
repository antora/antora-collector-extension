= Antora Collector Extension

The Antora Collector extension augments the content aggregation capabilities of Antora by invoking external commands that generate and/or retrieve files that can be imported into the content aggregate.

== Overview

The Antora Collector extension offers a configuration-based mechanism to contribute additional content to the content aggregate.
The extension allows you to delegate to zero or more external commands (e.g., `./gradlew generateContent`) that may generate and/or retrieve files.
The extension also provides a file scanner to find and import additional files (committed, generated, or downloaded) into the content aggregate (and, in turn, the content catalog).
These files then become referencable resources (e.g., pages, partials, attachments, etc.) in an Antora site.

You can think of Collector as adding additional content roots to your playbook that happen to be created dynamically (and which are transient).
The extension is agnostic to the build tool (Gradle, Maven, npm, etc.) and--to the extent possible--the operating system.

== Install

Use the following command to install the extension into your playbook project:

 $ npm i @antora/collector-extension

== Register

Open your Antora playbook file and register the extension as an entry in the `antora.extensions` key.
If this key doesn't yet exist in your playbook, first create it.

.antora-playbook.yml
[,yaml]
----
antora:
  extensions:
  - '@antora/collector-extension'
----

== Configure

To configure the extension, you must define at least one step with a clean, run, and/or scan action under the `ext.collector` key in a component descriptor.

.antora.yml
[,yaml]
----
name: my-project
version: '1.0'
# ...
ext:
  collector:
    run: generate-files.js
    scan: build/generated-files
----

== Copyright and License

Copyright (C) 2022-present by OpenDevise Inc. and the individual contributors of this project.

Use of this software is granted under the terms of the https://www.mozilla.org/en-US/MPL/2.0/[Mozilla Public License Version 2.0] (MPL-2.0).
