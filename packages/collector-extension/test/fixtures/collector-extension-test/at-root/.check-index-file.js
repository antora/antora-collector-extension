'use strict'

const fsp = require('node:fs/promises')
const ospath = require('node:path')

;(async () => {
  const gitdir = await fsp.readFile('.git/commondir', 'utf8').then((contents) => contents.trim())
  const exists = await fsp.access(ospath.join(gitdir, 'index')).then(
    () => true,
    () => false
  )
  await fsp.mkdir('build/modules/ROOT/pages', { recursive: true })
  if (exists) {
    await fsp.writeFile('build/modules/ROOT/pages/exists.adoc', '= Index Exists', 'utf8')
  } else {
    await fsp.writeFile('build/modules/ROOT/pages/does-not-exist.adoc', '= Index Does Not Exist', 'utf8')
  }
})()
