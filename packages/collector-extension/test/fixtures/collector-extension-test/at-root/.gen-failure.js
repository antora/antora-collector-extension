'use strict'

const fsp = require('node:fs/promises')
const { EOL } = require('node:os')

;(async () => {
  process.stderr.write('creating file in directory that does not exist' + EOL)
  process.stderr.write('we expect this to fail' + EOL)
  await fsp.writeFile('path/does/not/exist/file.adoc', 'content', 'utf8')
})()
