'use strict'

const fsp = require('node:fs/promises')
const { spawnSync } = require('node:child_process')

;(async () => {
  const dirty = spawnSync('git', ['status', '--short']).stdout.toString().trimRight()
  const branch = spawnSync('git', ['rev-parse', '--abbrev-ref', 'HEAD']).stdout.toString().trimRight()
  await fsp.writeFile('antora.yml', `name: test\nversion: '${branch.slice(1).replace('.x', '')}'\n`, 'utf8')
  await fsp.writeFile(`untracked-${branch}.txt`, 'untracked', 'utf8')
  if (process.argv[2] === 'delete') await fsp.rm(process.argv[1])
  await fsp.mkdir('build/modules/ROOT/pages', { recursive: true })
  await fsp.writeFile('build/antora.yml', `name: test\nversion: '${branch.slice(1).replace('.x', '')}'\n`, 'utf8')
  await fsp.writeFile(`build/modules/ROOT/pages/${branch}-release.adoc`, '= Branch-Specific Page', 'utf8')
  if (dirty) {
    fsp.writeFile('build/modules/ROOT/pages/dirty-worktree.adoc', `= Dirty Worktree\n\n....\n${dirty}\n....`, 'utf8')
  }
})()
