'use strict'

const fsp = require('node:fs/promises')
const yaml = require('js-yaml')

;(async () => {
  await fsp.mkdir('build/modules/ROOT/pages', { recursive: true })
  let contents
  if ('ENV_VALUE_AS_STRING' in process.env) {
    const envData = JSON.stringify({ ENV_VALUE_AS_STRING: process.env.ENV_VALUE_AS_STRING, USER: process.env.USER })
    contents = `= Environment Variables\n\n ${envData}`
  } else if ('refname' in process.env) {
    contents = `= Refname\n\n${process.env.refname}`
  } else {
    const origin = parseData(process.env.ANTORA_COLLECTOR_ORIGIN)
    const playbook = parseData(process.env.ANTORA_PLAYBOOK)
    contents = [
      '= Origin Info',
      '',
      'reftype:: ' + origin.reftype,
      'refname:: ' + origin.refname,
      'worktree:: ' + process.env.ANTORA_COLLECTOR_WORKTREE,
      'site title:: ' + process.env.ANTORA_COLLECTOR_SITE_TITLE,
      'site url:: ' + playbook.site.url,
      'env in playbook:: ' + ('env' in playbook),
    ].join('\n')
  }
  await fsp.writeFile('build/modules/ROOT/pages/index.adoc', contents, 'utf8')
})()

function parseData (data = '{}') {
  return data.charAt() === '{' ? JSON.parse(data) : yaml.load(data)
}
