'use strict'

const fsp = require('node:fs/promises')
const { spawnSync } = require('node:child_process')

;(async () => {
  const tag = spawnSync('git', ['describe', '--tags']).stdout.toString().trimRight()
  await fsp.mkdir('build/modules/ROOT/pages', { recursive: true })
  await fsp.writeFile('build/antora.yml', `name: test\nversion: '${tag.slice(1)}'\n`, 'utf8')
  await fsp.writeFile(`build/modules/ROOT/pages/${tag}-release.adoc`, '= Tag-Specific Page', 'utf8')
})()
