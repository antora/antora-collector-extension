'use strict'

const fsp = require('node:fs/promises')

;(async () => {
  await fsp.mkdir('build', { recursive: true })
  const antoraYml = "name: new-name\nversion: '1.0'\n"
  await fsp.writeFile('build/antora.yml', antoraYml, 'utf8')
})()
