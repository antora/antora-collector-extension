'use strict'

const fsp = require('node:fs/promises')

;(async () => {
  await fsp.mkdir('build/modules/ROOT/pages', { recursive: true })
  await fsp.writeFile('build/modules/ROOT/pages/index.adoc', '= Start Page', 'utf8')
})()
